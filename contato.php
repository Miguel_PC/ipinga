<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="favicon.ico">

  <title>Ipinga - bora!</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="carousel.css" rel="stylesheet">
</head>

<body id="home">

  <header>
    
    <?php
    include "menu.php";
    ?>
    
  </header>

  <div class="container marketing">
    
      <div class="featurette-divider"></div>
      <!-- Contato -->
      <section id="contato">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 text-center">
              <h2 class="section-heading text-uppercase">Contato</h2>
              <h3 class="section-subheading text-muted">Mande a sua dúvida ou sugestão</h3>
              <div class="featurette-divider"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">



              <form class="form" method="POST" action="./enviar.php">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input class="form-control" id="name" name="nome" type="text" placeholder="Nome" required="required"
                        data-validation-required-message="Preencha com o nome.">
                      <p class="help-block text-danger"></p>
                    </div>
                    <div class="form-group">
                      <input class="form-control" id="email" name="email" type="email" placeholder="Email" required="required"
                        data-validation-required-message="Preencha com o email.">
                      <p class="help-block text-danger"></p>
                    </div>
                    <div class="form-group">
                      <input class="form-control" id="phone" name="telefone" type="telefone" placeholder="Telefone" required="required"
                        data-validation-required-message="Preencha com o telefone.">
                      <p class="help-block text-danger"></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <textarea class="form-control" id="message" name="mensagem" placeholder="Mensagem" required="required"
                        data-validation-required-message="Preencha com a sua mensagem."></textarea>
                      <p class="help-block text-danger"></p>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-lg-12 text-center">
                    <div id="success"></div>
                    <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase"
                      type="submit">Enviar</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
      <!--FIM contato-->
      <div class="featurette-divider"></div>



  </div><!-- /.container -->


  <!-- FOOTER -->
  <footer class="container">
    <p class="float-right"><a href="#">Votlar ao Topo</a></p>
    <p>&copy; 2019 Ipinga</p>
  </footer>
  </main>

  <!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
  <script src="js/holder.min.js"></script>
</body>

</html>
