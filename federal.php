<!doctype html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="favicon.ico">

  <title>Ipinga - bora!</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="carousel.css" rel="stylesheet">
</head>

<body id="home">

<header>
    
    <?php
    include "menu.php";
    ?>

  </header>

  <div class="featurette-divider"></div>

  <div class="container marketing">
    <main role="main">
      <!-- carousel-->
      <div class="td-post-content">
          <div class="td-post-featured-image"><img width="100%" height="400" class="entry-thumb" src="https://agitabrasilia.com/wp-content/uploads/2019/04/federal-music-brasilia-2019-696x364.jpg" srcset="https://agitabrasilia.com/wp-content/uploads/2019/04/federal-music-brasilia-2019.jpg 696w, https://agitabrasilia.com/wp-content/uploads/2019/04/federal-music-brasilia-2019-300x157.jpg 300w" sizes="(max-width: 696px) 100vw, 696px" alt="" title="federal-music-brasilia-2019" /></div>

          
          <p>Federal Music 2019 , Após 10 anos de dedicação a cena de música eletrônica do centro-oeste e mais de 11 edições entre especiais e as principais, orgulhosamente apresentamos oficialmente o <a href="https://agitabrasilia.com/federal-music-brasilia-2018/">Federal Music 2019.</a></p>
          <p><strong>Data:</strong> 11 outubro 2019, quinta-feira.<br />
          <strong>Hora:</strong> 16 horas<br />
          <strong>Local: </strong>Torre de TV Digital &#8211; Brasilia, DF</p>
          <p><strong>Atrações</strong></p>
          <p>Primeiras Atrações Confirmadas</p>
          <ul>
          <li><b>Astrix </b>(Kontrol / Dm7)<br />
          <b></b></li>
          <li><b>Illusionize </b>(Plusnetwork)</li>
          </ul>
          <p><strong>Ingressos</strong></p>
          <p><b>Pré-Venda + Copo + Pulseira</b></p>
          <ul>
          <li>R$50,00 Front Stage</li>
          <li>R$80,00 Camarote</li>
          </ul>
          <p><strong>Pontos de Vendas</strong></p>
          <ul>
          <li><strong>Vendas Online</strong>: <a href="https://www.sympla.com.br/federal-music-2019---reino-de-omnia__489808?fbclid=IwAR3O4JXvesowEFnITHb1aLoRSUElHAUOxm5mTnUe-mv4h928WqX_jRGlMYU" target="_blank" rel="noopener noreferrer">https://www.sympla.com.br/</a></li>
          </ul>
          <p><strong>Mais Informações</strong></p>
          <ul>
          <li><strong>Telefone:</strong> Não Informado.</li>
          <li><strong>Classificação:</strong> Não Informado.</li>
          </ul>
          </div>
      <!--FIM carousel-->


      <!-- mapa-->
      <div class="featurette-divider" id="mapa">
        <div class="row">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3840.9795064203363!2d-47.83168788607399!3d-15.699238989110444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3edba6e3e967%3A0xe68de5e1ce39af52!2sTorre+de+TV+Digital!5e0!3m2!1spt-BR!2sbr!4v1559826115888!5m2!1spt-BR!2sbr" 
            width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

          </div>
        </div>

      <!--FIM mapa-->

      <div class="featurette-divider" id="locais"></div>

      <div style="padding-bottom:30px" class="col-lg-12 text-center">
        <h2 class="section-heading text-uppercase">Locais</h2>
      </div>


<!--Locais-->
      <div class="row">
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/drinks.png" alt="Generic placeholder image" width="140" height="140">
          <h2>Distribuidora Drink's</h2>
          <p>Tabacaria, bebidas e Conveniência</p>
          <p><a class="btn btn-success" href="#" role="button">Onde fica</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/informal.png" alt="Generic placeholder image" width="140"
            height="140">
          <h2>Informal Bebidas</h2>
          <p>Cervejas nacional, artesanal e Importadas</p>
          <p><a class="btn btn-success" href="#" role="button">Onde Fica</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/ze.png" alt="Generic placeholder image" width="140" height="140">
          <h2>Delivery de Bebidas</h2>
          <p>Cerveja, carvão e gelo</p>
          <p><a class="btn btn-success" href="#" role="button">Onde Fica</a></p>
        </div>
      </div>
<!--FIM Locais-->

  </div><!-- /.container -->


  <!-- FOOTER -->
  <footer class="container">
    <p class="float-right"><a href="#">Votlar ao Topo</a></p>
    <p>&copy; 2019 Ipinga</p>
  </footer>
  </main>

  <!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
  <script src="js/holder.min.js"></script>
</body>

</html>
