<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="favicon.ico">

  <title>Ipinga - bora!</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="carousel.css" rel="stylesheet">
</head>

<body id="home">

<header>
    
    <?php
    include "menu.php";
    ?>

  </header>

  <div class="container marketing">

      <!--Locais-->      
      <div class="featurette-divider" id="locais"></div>

      <div style="padding-bottom:30px" class="col-lg-12 text-center">
        <h2 class="section-heading text-uppercase">Locais</h2>
      </div>

      
      <div class="row">
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/drinks.png" alt="Generic placeholder image" width="140" height="140">
          <h2>Distribuidora Drink's</h2>
          <p>Tabacaria, bebidas e Conveniência</p>
          <p><a class="btn btn-success" href="#" role="button">Onde fica</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/informal.png" alt="Generic placeholder image" width="140"
            height="140">
          <h2>Informal Bebidas</h2>
          <p>Cervejas nacional, artesanal e Importadas</p>
          <p><a class="btn btn-success" href="#" role="button">Onde Fica</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/ze.png" alt="Generic placeholder image" width="140" height="140">
          <h2>Delivery de Bebidas</h2>
          <p>Cerveja, carvão e gelo</p>
          <p><a class="btn btn-success" href="#" role="button">Onde Fica</a></p>
        </div>
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/biritas.png" alt="Generic placeholder image" width="140" height="140">
          <h2>Disk Biritas</h2>
          <p>Bebidas nacionais e importadas</p>
          <p><a class="btn btn-success" href="#" role="button">Onde Fica</a></p>
        </div>
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/miguelito.png" alt="Generic placeholder image" width="140"
            height="140">
          <h2>miguelito</h2>
          <p>Corote geladinho, R$2,99</p>
          <p><a class="btn btn-success" href="#" role="button">Onde Fica</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/beer.png" alt="Generic placeholder image" width="140" height="140">
          <h2>Beer Happy</h2>
          <p>Cerveja artesanal</p>
          <p><a class="btn btn-success" href="#" role="button">Onde fica</a></p>
        </div><!-- /.col-lg-4 -->
      </div>
      <!--FIM Locais-->



  </div>
  <!-- /.container -->


  <!-- FOOTER -->
  <footer class="container">
    <p class="float-right"><a href="#">Votlar ao Topo</a></p>
    <p>&copy; 2019 Ipinga</p>
  </footer>
  </main>

  <!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
  <script src="js/holder.min.js"></script>
</body>

</html>