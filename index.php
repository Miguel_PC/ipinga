<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="favicon.ico">
  <link href ="css/style.css" rel="stylesheet">
  <title>Ipinga - bora!</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="carousel.css" rel="stylesheet">
</head>

<body id="home">

  <header>
    
    <?php
    include "menu.php";
    ?>
    
  </header>



      <!-- carousel-->
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="second-slide" src="images/magic.PNG" alt="Magic">
            <div class="container">
              <div class="carousel-caption">
                <p><a class="btn btn-lg btn-warning" href="magic.php" role="button">Saiba mais</a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item ">
            <img class="first-slide" src="images/federal.PNG" alt="Federal">
            <div class="container">
              <div class="carousel-caption">
                <p><a class="btn btn-lg btn-danger " href="federal.php" role="button">Saiba mais</a></p>
              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      <!--FIM carousel-->

      <div class="container marketing">
      <!-- mapa-->


        <!-- Busca no mapa -->
        <!-- 
              <form class="form-inline col-lg-10">
                <input class="form-control col-lg-4 text-center" type="text"  aria-label="digitar">
                <button class="btn btn-success col-lg-4 text-center" type="submit">Procurar</button>
              </form>

        <-- Fim Busca no mapa --
        <div style="padding-bottom:30px" class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Mapa</h2>
        </div>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3838.4104139600436!2d-48.043938821067805!3d-15.835012413484808!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a32f36c5fa937%3A0x173f55038a986050!2sUniCEUB+-+Taguatinga+Campus+II!5e0!3m2!1spt-BR!2sbr!4v1557882000526!5m2!1spt-BR!2sbr"
              width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>-->

<!--Search bar do mapa-->

      <div class="row mb-2">
        <div class="col">
      <div class="featurette-divider"></div>
          <input class="form-control" id="search" type="text" placeholder="Search..." />
        </div>
      </div>
      <!--Fim search bar do mapa-->
      <!-- mapa-->
      <div class="row">
        <div class="col">
          <div id="map"></div>
        </div>
      </div>
        
      
      
      <!--FIM mapa-->


      <!--FIM mapa-->

      <div class="featurette-divider" id="locais"></div>

      <div style="padding-bottom:30px" class="col-lg-12 text-center">
        <h2 class="section-heading text-uppercase">Locais</h2>
      </div>

<!--Locais-->
      <div class="row">
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/drinks.png" alt="Generic placeholder image" width="140" height="140">
          <h2>Distribuidora Drink's</h2>
          <p>Tabacaria, bebidas e Conveniência</p>
          <p><a class="btn btn-success" href="#" role="button">Onde fica</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/informal.png" alt="Generic placeholder image" width="140"
            height="140">
          <h2>Informal Bebidas</h2>
          <p>Cervejas nacional, artesanal e Importadas</p>
          <p><a class="btn btn-success" href="#" role="button">Onde Fica</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/ze.png" alt="Generic placeholder image" width="140" height="140">
          <h2>Delivery de Bebidas</h2>
          <p>Cerveja, carvão e gelo</p>
          <p><a class="btn btn-success" href="#" role="button">Onde Fica</a></p>
        </div>
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/biritas.png" alt="Generic placeholder image" width="140" height="140">
          <h2>Disk Biritas</h2>
          <p>Tele entraga e cervejas artesanais</p>
          <p><a class="btn btn-success" href="#" role="button">Onde Fica</a></p>
        </div>
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/miguelito.png" alt="Generic placeholder image" width="140"
            height="140">
          <h2>Miguelito</h2>
          <p>Corote geladinho, R$2,99</p>
          <p><a class="btn btn-success" href="#" role="button">Onde Fica</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/beer.png" alt="Generic placeholder image" width="140" height="140">
          <h2>Beer Happy</h2>
          <p>Cerveja artesanal</p>
          <p><a class="btn btn-success" href="#" role="button">Onde fica</a></p>
        </div><!-- /.col-lg-4 -->
      </div>
<!--FIM Locais-->

      <div class="featurette-divider"></div>
      <!-- Contato -->
      <section id="contato">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 text-center">
              <h2 class="section-heading text-uppercase">Contato</h2>
              <h3 class="section-subheading text-muted">Mande a sua dúvida ou sugestão</h3>
              <div class="featurette-divider"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">



              <form class="form" method="POST" action="./enviar.php">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input class="form-control" id="name" name="nome" type="text" placeholder="Nome" required="required"
                        data-validation-required-message="Preencha com o nome.">
                      <p class="help-block text-danger"></p>
                    </div>
                    <div class="form-group">
                      <input class="form-control" id="email" name="email" type="email" placeholder="Email" required="required"
                        data-validation-required-message="Preencha com o email.">
                      <p class="help-block text-danger"></p>
                    </div>
                    <div class="form-group">
                      <input class="form-control" id="phone" name="telefone" type="telefone" placeholder="Telefone" required="required"
                        data-validation-required-message="Preencha com o telefone.">
                      <p class="help-block text-danger"></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <textarea class="form-control" id="message" name="mensagem" placeholder="Mensagem" required="required"
                        data-validation-required-message="Preencha com a sua mensagem."></textarea>
                      <p class="help-block text-danger"></p>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-lg-12 text-center">
                    <div id="success"></div>
                    <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase"
                      type="submit">Enviar</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
      <!--FIM contato-->
      <div class="featurette-divider"></div>



  </div><!-- /.container -->


  <!-- FOOTER -->
  <footer class="container">
    <p class="float-right"><a href="#">Votlar ao Topo</a></p>
    <p>&copy; 2019 Ipinga</p>
  </footer>
  </main>

  <!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
  <script src="js/holder.min.js"></script>


  <!--Google Maps API scripts-->
  <script src="js/script.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?callback=createMap&libraires=places&key=AIzaSyDp79NE_AWj1Hna2K6L0zMpQqomTFTgza8"></script>
</body>

</html>
