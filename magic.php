<!doctype html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="favicon.ico">

  <title>Ipinga - bora!</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="carousel.css" rel="stylesheet">
</head>

<body id="home">

<header>
    
    <?php
    include "menu.php";
    ?>

  </header>

  <div class="featurette-divider"></div>

  <div class="container marketing">
    <main role="main">
      <!-- carousel-->
      <div class="td-post-content">
          <div class="td-post-featured-image"><img width="100%" height="400" class="entry-thumb" src="https://agitabrasilia.com/wp-content/uploads/2019/05/5cacce5b959f6-lg-696x364.png" title="federal-music-brasilia-2019" /></div>

          
        <div class="descricao-interno-post col-md-8">
<p>Dia 8 de junho vai rolar Magic Island Brasília, a maior festa à fantasia do Brasil.</p>
</div>
<div class="descricao-interno-post col-md-8"><b>Data: </b>8 de Junho de 2019, Sábado<br />
<b>Hora: </b>22 horas<br />
<b>Local: </b>Estádio Nacional Mané Garrincha</div>
<div class="selo-foto hidden-xs">
<div id="div-gpt-ad-1433161902414-1" data-google-query-id="CMKYrPihv-ICFcx0wQodHowNiA"></div>
</div>
<h2><strong>Magic Island Brasília</strong></h2>
<p>A maior festa à fantasia do Brasil acontece na ilha mais badalada do país! E Brasília que sempre teve essa tradição, há muito tempo espera uma grande festa à fantasia novamente. Então, o que acham de juntar a maior festa à fantasia com essa longa espera?</p>
<p>Senhoras e senhores: Preparem seus figurinos, preparem os esqueletos, a espera chegou ao fim!</p>
<h3><strong>Atrações</strong></h3>
<ul>
<li>Chapeleiro (Dm7)</li>
<li>Liu (Artist Factory)</li>
<li>VINNE (Plus Talent)</li>
<li>SANTTI (Plus Talent)</li>
<li>Clubbers (Artist Factory)</li>
<li>FreaKaholics (Mantra)</li>
<li>Áquila</li>
<li>Legan</li>
<li>Galvik</li>
<li>Kevin Chris</li>
<li>+ artistas a confirmar</li>
</ul>
<h3><strong>Ingressos</strong></h3>
<p><strong>1° Lote</strong></p>
<p>Front Stage</p>
<ul>
<li>R$ 70,00 + taxa</li>
</ul>
<p>Camarote</p>
<ul>
<li>R$ 100,00 + taxa</li>
</ul>
<p>Combo Amigo Front Stage</p>
<ul>
<li>R$ 56,00 + taxa</li>
</ul>
<p>Combo Amigo Camarote</p>
<ul>
<li>R$ 80,00 + taxa</li>
</ul>
<p>*Valores dos ingressos sujeitos à alterações sem aviso prévio.</p>
<h3><strong>Pontos de Venda</strong></h3>
<ul>
<li><a href="https://www.sympla.com.br/magic-island-brasilia--a-maior-festa-a-fantasia-do-brasil__408161" target="_blank" rel="noopener noreferrer">Site Sympla</a></li>
</ul>
<p><strong>Físico</strong> (Dinheiro ou cartão com taxa)</p>
<ul>
<li>Taguatinga Shopping – Loja da Claro 2º piso ao lado da Riachuelo</li>
<li>Park Shoppung – Loja da Claro 2º piso ao lado da Cia Toy</li>
<li>Conjunto Nacional – Loja da Claro Subsolo em frente a Marisa</li>
<li>Jk Shopping – Visual Calçado</li>
<li>Santa Maria Shopping – Stilus Hering 2º Piso</li>
<li>Big Lancher Setor Comercial Sul em frente ao Bradesco</li>
<li>Selfie Mobile em Frente do na Hora</li>
<li>Tabacaria Condis – Paranoá</li>
<li>Arte em Fantasia – Candangolândia</li>
<li>Camarim Fantasias – Asa Sul</li>
<li>Fantasias.com – Taguatinga Sul</li>
<li>Luxo e Fantasia – Asa Norte</li>
<li>Criativa Fantasias – Taguatinga Norte</li>
<li>Casa e Festa – Asa sul, Asa Norte e Águas Claras, Taguatinga Norte, Gama e Samambaia</li>
<li>World Piercing Tonny – Paranoá</li>
<li>135 Store – Sudoeste 101 ao lado da Koni</li>
</ul>
<h3><strong>Mais Informações</strong></h3>
<ul>
<li><strong>Telefone:</strong> (61) 98427-7309</li>
<li><strong>Classificação: </strong>18 anos</li>
</ul>
      <!--FIM carousel-->


      <!-- mapa-->
      <div class="featurette-divider" id="mapa">
        <div class="row">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3839.387303956291!2d-47.9013996851452!3d-15.783519089058041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3af5eeb75937%3A0xfd6e9aa239a3075b!2sEst%C3%A1dio+Man%C3%A9+Garrincha!5e0!3m2!1spt-BR!2sbr!4v1559870676375!5m2!1spt-BR!2sbr" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

          </div>
        </div>

      <!--FIM mapa-->

      <div class="featurette-divider" id="locais"></div>

      <div style="padding-bottom:30px" class="col-lg-12 text-center">
        <h2 class="section-heading text-uppercase">Locais</h2>
      </div>


<!--Locais-->
      <div class="row">
      <div class="col-lg-4">
          <img class="rounded-circle" src="images/biritas.png" alt="Generic placeholder image" width="140" height="140">
          <h2>Disk Biritas</h2>
          <p>Bebidas nacionais e importadas</p>
          <p><a class="btn btn-success" href="#" role="button">Onde Fica</a></p>
        </div>
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/miguelito.png" alt="Generic placeholder image" width="140"
            height="140">
          <h2>miguelito</h2>
          <p>Corote geladinho, R$2,99</p>
          <p><a class="btn btn-success" href="#" role="button">Onde Fica</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="rounded-circle" src="images/beer.png" alt="Generic placeholder image" width="140" height="140">
          <h2>Beer Happy</h2>
          <p>Cerveja artesanal</p>
          <p><a class="btn btn-success" href="#" role="button">Onde fica</a></p>
        </div><!-- /.col-lg-4 -->
      </div>
<!--FIM Locais-->

  </div><!-- /.container -->


  <!-- FOOTER -->
  <footer class="container">
    <p class="float-right"><a href="#">Votlar ao Topo</a></p>
    <p>&copy; 2019 Ipinga</p>
  </footer>
  </main>

  <!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
  <script src="js/holder.min.js"></script>
</body>

</html>
