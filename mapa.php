<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="favicon.ico">
  
  <link href ="css/style.css" rel="stylesheet">

  <title>Ipinga - bora!</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="carousel.css" rel="stylesheet">
</head>

<body id="home">

  <header>
    
    <?php
    include "menu.php";
    ?>
    
  </header>


  <div class="row mb-2">
        <div class="col">
      <div class="featurette-divider"></div>
          <input class="form-control" id="search" type="text" placeholder="Search..." />
        </div>
      </div>
      <!--Fim search bar do mapa-->
      <!-- mapa-->
      <div class="row">
        <div class="col">
          <div id="map"></div>
        </div>
      </div>


  <!-- FOOTER -->
  </main>

  <!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
  <script src="js/holder.min.js"></script>
   <!--Google Maps API scripts-->
   <script src="js/script.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?callback=createMap&libraires=places&key=AIzaSyA2Kp14n3ENra8_qkseyb_CtdlrYrNd2Ng"></script>

</body>

</html>
